export enum UserCriteriaType {
    NICE = "NICE",
    SUPER = "SUPER", 
    MUST = "MUST"
}