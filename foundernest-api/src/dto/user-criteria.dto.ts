import { ApiProperty } from "@nestjs/swagger";
import { UserCriteriaType } from "./user-criteria-type.enum";

export class UserCriteriaDTO {
    @ApiProperty()
    public name: string;
    @ApiProperty()
    public type: UserCriteriaType
}