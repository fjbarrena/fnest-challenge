import { Injectable } from '@nestjs/common';
import { UserCriteriaDTO } from 'src/dto/user-criteria.dto';
import { UserDAO } from '../../dao/user/user-dao.service';

@Injectable()
export class UserService {
    constructor(
        private readonly userDAO: UserDAO
    ) { }

    async getUserCriteria(userId: string): Promise<UserCriteriaDTO[]> {
        // Here we will apply core/business checks, like
        //      * Has the user the adequate righs?
        //      * The provided data is correct?
        //      * And here we can apply consistent error checking

        // In this exercise we will assume that we don't have any check or operation to do
        // So we can return the data direct from the DAO layer
        return this.userDAO.getUserCriteria(userId);        
    }
}
