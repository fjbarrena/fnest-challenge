import { Injectable } from '@nestjs/common';
import { CriteriaDAO } from '../../dao/criteria/criteria-dao.service';

@Injectable()
export class CriteriaService {
    constructor(
        private readonly criteriaDAO: CriteriaDAO
    ) { }
}
