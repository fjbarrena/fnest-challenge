import { Injectable } from '@nestjs/common';
import { DecisionDAO } from '../../dao/decision/decision-dao.service';

@Injectable()
export class DecisionService {
    constructor(
        private readonly decisionDAO: DecisionDAO
    ) { }
}
