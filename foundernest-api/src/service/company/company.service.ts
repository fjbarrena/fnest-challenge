import { Injectable } from '@nestjs/common';
import { CompanyDAO } from '../../dao/company/company-dao.service';

@Injectable()
export class CompanyService {
    constructor(
        private readonly companyDAO: CompanyDAO
    ) { }
}
