import { Module } from '@nestjs/common';
import { CompanyService } from './service/company/company.service';
import { CriteriaService } from './service/criteria/criteria.service';
import { DecisionService } from './service/decision/decision.service';
import { UserController } from './controller/user/user.controller';
import { UserService } from './service/user/user.service';
import { CompanyDAO } from './dao/company/company-dao.service';
import { CriteriaDAO } from './dao/criteria/criteria-dao.service';
import { DecisionDAO } from './dao/decision/decision-dao.service';
import { UserDAO } from './dao/user/user-dao.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from './entity/company';
import { Criteria } from './entity/criteria';
import { Decision } from './entity/decision';
import { Reasons } from './entity/reasons';
import { UserCompanyMatching } from './entity/user-company-matching';
import { UserHasCriteria } from './entity/user-has-criteria';

@Module({
  imports: [
    // That should be extracted to an environment file ;)
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'foundernest-db',
      port: 3306,
      username: 'root',
      password: 'secret',
      database: 'foundernest',
      entities: [
        Company, 
        Criteria, 
        Decision, 
        Reasons, 
        UserCompanyMatching, 
        UserHasCriteria
      ],
      synchronize: false
    })
  ],
  controllers: [
    UserController
  ],
  providers: [
    // Core
    CompanyService,
    CriteriaService, 
    DecisionService,
    UserService,

    // Data Access layer
    CompanyDAO, 
    CriteriaDAO,
    DecisionDAO,
    UserDAO
  ],
})
export class AppModule {}
