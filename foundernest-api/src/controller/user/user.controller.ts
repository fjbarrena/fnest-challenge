import { Controller, Get, HttpStatus, Param, Post, Headers } from '@nestjs/common';
import { ApiBearerAuth, ApiHeader, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GenericController } from '../generic.controller';
import { UserService } from '../../service/user/user.service';
import { UserCriteriaDTO } from '../../dto/user-criteria.dto';

@Controller('user')
@ApiTags('user')
@ApiBearerAuth()
export class UserController extends GenericController {
    constructor(private readonly userService: UserService) {
        super();
    }

    @Get(':userId/criteria')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiResponse({ status: 200, description: 'List of the criteria configured by the user', type: UserCriteriaDTO})
    @ApiResponse({ status: 204, description: 'No records found', type: UserCriteriaDTO})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async getUserCriteria(
        @Param('userId') userId,
        @Headers() headers
    ): Promise<UserCriteriaDTO[]> {
        const token = headers.authorization;
        // In a real environment I would check the token vs the security API to ensure that the token is legit
        console.log(token);
        // Once we're sure that the token is legit, we can extract the info inside the token and pass throught the
        // service layer to make some operations, checkings, etc.

        return await this.userService.getUserCriteria(userId);;
    }

    @Get('/:userId/company/decision/waiting')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiResponse({ status: 200, description: 'List of the companies waiting for decision by the user'})
    @ApiResponse({ status: 204, description: 'No records found'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async getCompaniesWaitingForDecision(@Param('userId') userId): Promise<any[]> {
        // STUB
        return [];
    }

    @Get('/:userId/company/decision/firstMeeting')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiResponse({ status: 200, description: 'List of the companies moved to first meeting by the user'})
    @ApiResponse({ status: 204, description: 'No records found'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async getCompaniesMovedToFirstMeeting(@Param('userId') userId): Promise<any[]> {
        // STUB
        return [];
    }

    @Get('/:userId/company/decision/discarded')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiResponse({ status: 200, description: 'List of the companies discarded by the user'})
    @ApiResponse({ status: 204, description: 'No records found'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async getCompaniesDiscarded(@Param('userId') userId): Promise<any[]> {
        // STUB
        return [];
    }

    @Post('/:userId/company/:companyId/meet')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiParam({name: 'companyId', required: true, description: 'Company UUID'})
    @ApiResponse({ status: 201, description: 'Operation success'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async meetCompany(@Param('userId') userId, @Param('companyId') companyId): Promise<any[]> {
        // STUB
        return [];
    }

    @Post('/:userId/company/:companyId/pass')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiParam({name: 'companyId', required: true, description: 'Company UUID'})
    @ApiResponse({ status: 201, description: 'Operation success'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async passCompany(@Param('userId') userId, @Param('companyId') companyId): Promise<any[]> {
        // STUB
        return [];
    }

    @Post('/:userId/criteria')
    @ApiParam({name: 'userId', required: true, description: 'User UUID'})
    @ApiResponse({ status: 201, description: 'Operation success'})
    @ApiResponse({ status: 500, description: 'Generic error'})
    async updateUserCriteria(@Param('userId') userId, newCriteria): Promise<any[]> {
        // STUB
        return [];
    }
}
