import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
} from "typeorm";
import { UserCompanyMatching } from "./user-company-matching";
import { Reasons } from "./reasons";

@Index("fk_decision_user_company_matching1_idx", ["UserCompanyMatchingId"], {})
@Entity("decision", { schema: "foundernest" })
export class Decision {
  @Column("char", {
    primary: true,
    name: "id",
    length: 36,
    default: () => "'uuid()'",
  })
  Id: string;

  @Column("char", { name: "user_company_matching_id", length: 36 })
  UserCompanyMatchingId: string;

  @Column("tinyint", { name: "meet" })
  Meet: number;

  @Column("datetime", {
    name: "createdDate",
    default: () => "CURRENT_TIMESTAMP",
  })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @ManyToOne(
    () => UserCompanyMatching,
    (UserCompanyMatching) => UserCompanyMatching.Decisions,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([
    { name: "user_company_matching_id", referencedColumnName: "Id" },
  ])
  UserCompanyMatching: UserCompanyMatching;

  @ManyToMany(() => Reasons, (Reasons) => Reasons.Decisions)
  @JoinTable({
    name: "decision_has_reasons",
    joinColumns: [{ name: "decision_id", referencedColumnName: "Id" }],
    inverseJoinColumns: [{ name: "reasons_id", referencedColumnName: "Id" }],
    schema: "foundernest",
  })
  Reasons: Reasons[];
}
