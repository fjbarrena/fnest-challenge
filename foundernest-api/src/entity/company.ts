import { Column, Entity, JoinTable, ManyToMany, OneToMany } from "typeorm";
import { Criteria } from "./criteria";
import { UserCompanyMatching } from "./user-company-matching";

@Entity("company", { schema: "foundernest" })
export class Company {
  @Column("char", {
    primary: true,
    name: "id",
    length: 36,
    default: () => "'uuid()'",
  })
  Id: string;

  @Column("varchar", { name: "name", length: 255 })
  Name: string;

  @Column("datetime", { name: "createdDate" })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @ManyToMany(() => Criteria, (Criteria) => Criteria.Companies)
  @JoinTable({
    name: "company_has_criteria",
    joinColumns: [{ name: "company_id", referencedColumnName: "Id" }],
    inverseJoinColumns: [{ name: "criteria_id", referencedColumnName: "Id" }],
    schema: "foundernest",
  })
  Criteria: Criteria[];

  @OneToMany(
    () => UserCompanyMatching,
    (UserCompanyMatching) => UserCompanyMatching.Company
  )
  UserCompanyMatchings: UserCompanyMatching[];
}
