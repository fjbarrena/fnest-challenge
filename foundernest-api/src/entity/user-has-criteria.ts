import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Criteria } from "./criteria";

@Index("fk_user_has_criteria_criteria1_idx", ["CriteriaId"], {})
@Entity("user_has_criteria", { schema: "foundernest" })
export class UserHasCriteria {
  @Column("char", { primary: true, name: "criteria_id", length: 36 })
  CriteriaId: string;

  @Column("char", { primary: true, name: "user_id", length: 36 })
  UserId: string;

  @Column("enum", { name: "type", enum: ["MUST", "SUPER", "NICE"] })
  Type: "MUST" | "SUPER" | "NICE";

  @Column("datetime", {
    name: "createdDate",
    default: () => "CURRENT_TIMESTAMP",
  })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @ManyToOne(() => Criteria, (Criteria) => Criteria.UserHasCriteria, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "criteria_id", referencedColumnName: "Id" }])
  Criteria: Criteria;
}
