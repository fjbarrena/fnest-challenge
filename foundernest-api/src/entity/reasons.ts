import { Column, Entity, ManyToMany } from "typeorm";
import { Decision } from "./decision";

@Entity("reasons", { schema: "foundernest" })
export class Reasons {
  @Column("char", {
    primary: true,
    name: "id",
    length: 36,
    default: () => "'uuid()'",
  })
  Id: string;

  @Column("tinytext", { name: "reason" })
  Reason: string;

  @Column("enum", { name: "type", enum: ["POSITIVE", "NEGATIVE"] })
  Type: "POSITIVE" | "NEGATIVE";

  @Column("datetime", {
    name: "createdDate",
    default: () => "CURRENT_TIMESTAMP",
  })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @ManyToMany(() => Decision, (Decision) => Decision.Reasons)
  Decisions: Decision[];
}
