import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Decision } from "./decision";
import { Company } from "./company";

@Index("fk_user_company_matching_company1_idx", ["CompanyId"], {})
@Entity("user_company_matching", { schema: "foundernest" })
export class UserCompanyMatching {
  @Column("char", {
    primary: true,
    name: "id",
    length: 36,
    default: () => "'uuid()'",
  })
  Id: string;

  @Column("char", { name: "company_id", length: 36 })
  CompanyId: string;

  @Column("char", { name: "user_id", length: 36 })
  UserId: string;

  @Column("json", { name: "matching" })
  Matching: object;

  @Column("datetime", {
    name: "createdDate",
    default: () => "CURRENT_TIMESTAMP",
  })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @OneToMany(() => Decision, (Decision) => Decision.UserCompanyMatching)
  Decisions: Decision[];

  @ManyToOne(() => Company, (Company) => Company.UserCompanyMatchings, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "company_id", referencedColumnName: "Id" }])
  Company: Company;
}
