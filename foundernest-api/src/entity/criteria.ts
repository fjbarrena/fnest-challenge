import { Column, Entity, ManyToMany, OneToMany } from "typeorm";
import { Company } from "./company";
import { UserHasCriteria } from "./user-has-criteria";

@Entity("criteria", { schema: "foundernest" })
export class Criteria {
  @Column("char", {
    primary: true,
    name: "id",
    length: 36,
    default: () => "'uuid()'",
  })
  Id: string;

  @Column("tinytext", { name: "name" })
  Name: string;

  @Column("datetime", {
    name: "createdDate",
    default: () => "CURRENT_TIMESTAMP",
  })
  CreatedDate: Date;

  @Column("datetime", { name: "lastModifiedDate" })
  LastModifiedDate: Date;

  @Column("char", { name: "lastModifiedUser", length: 36 })
  LastModifiedUser: string;

  @ManyToMany(() => Company, (Company) => Company.Criteria)
  Companies: Company[];

  @OneToMany(
    () => UserHasCriteria,
    (UserHasCriteria) => UserHasCriteria.Criteria
  )
  UserHasCriteria: UserHasCriteria[];
}
