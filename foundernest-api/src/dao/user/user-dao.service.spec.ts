import { Test, TestingModule } from '@nestjs/testing';
import { UserDAO } from './user-dao.service';

describe('UserDAO', () => {
  let service: UserDAO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserDAO],
    }).compile();

    service = module.get<UserDAO>(UserDAO);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
