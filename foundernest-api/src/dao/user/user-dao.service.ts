import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { UserHasCriteria } from '../../entity/user-has-criteria';
import { UserCompanyMatching } from '../../entity/user-company-matching';
import { Connection, Repository } from 'typeorm';
import { UserCriteriaDTO } from '../../dto/user-criteria.dto';

@Injectable()
export class UserDAO {
    // Dependency Injection
    constructor(
        private readonly connection: Connection
    ) { }

    async getUserCriteria(userId: string): Promise<UserCriteriaDTO[]> {
        // I prefer to use native SQL Queries, as are quicker and more clear than the different ORM APIs
        // Less magic, better understanding
        // But I like to use an ORM for other reasons (connection pool, better security, etc.), and sometimes there are
        // some features interesting to use for specific cases
        return this.connection.query(`
            SELECT c.\`name\`, userHasCriteria.\`type\`
            FROM criteria c
            JOIN (
                SELECT uh.criteria_id, uh.\`type\` FROM user_has_criteria uh
                WHERE uh.user_id = ?
            ) userHasCriteria
            ON c.id = userHasCriteria.criteria_id;    
        `, [userId]);
    }
}
