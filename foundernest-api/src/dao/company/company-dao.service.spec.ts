import { Test, TestingModule } from '@nestjs/testing';
import { CompanyDAO } from './company-dao.service';

describe('COmpanyDAO', () => {
  let service: CompanyDAO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CompanyDAO],
    }).compile();

    service = module.get<CompanyDAO>(CompanyDAO);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
