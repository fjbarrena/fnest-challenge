import { Test, TestingModule } from '@nestjs/testing';
import { DecisionDAO } from './decision-dao.service';

describe('DecisionDAO', () => {
  let service: DecisionDAO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DecisionDAO],
    }).compile();

    service = module.get<DecisionDAO>(DecisionDAO);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
