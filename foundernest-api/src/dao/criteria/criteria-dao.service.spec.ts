import { Test, TestingModule } from '@nestjs/testing';
import { CriteriaDAO } from './criteria-dao.service';

describe('CriteriaDAO', () => {
  let service: CriteriaDAO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CriteriaDAO],
    }).compile();

    service = module.get<CriteriaDAO>(CriteriaDAO);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
