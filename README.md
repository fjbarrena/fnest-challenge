# FounderNest Challenge

[Estimation](https://infrequent-boat-8ab.notion.site/FounderNest-Challenge-4fbe653c0c0f406bb7c3417ba6d3c8be)

## Assumptions

- User management is placed in other microservice
- User authentication and authorization is managed by an API Gateway or similar, so if our microservice receives a request, we assume it's legit
    - In a real environment, I prefer to double check the credentials, asking directly to the security microservice to ensure that the API Gateway was not compromised
- Our microservice receives a JWT token, in which the user UUID who makes the request is placed
- The communication between microservices is done using a message broker (RabbitMQ, Kafka, whatever...)
- There is another process who is the responsible to make the matchings and save them into the database

## Database model

- Primary keys are UUID() to avoid security issues related to enumeration attacks
- I choose a relational database to enforce data consistency

This is the database design for this microservice

![FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled.png](FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled.png)

## API

The code of the API is built on NestJS and is available at the following Git repository

## Run the code

Just download the repository and execute the following command:

```bash
docker-compose up
```

You should see something like this:

![FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled%201.png](FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled%201.png)

This command deploys a database with some data and the API is available at [http://localhost:3000/api](http://localhost:3000/api) 

Only the following web service is implemented:

![FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled%202.png](FounderNest%20Challenge%20e325efadf541483fa3abb0633ca1c61c/Untitled%202.png)

You can use the following identifier 05517a10-7e4d-4509-9ffb-57978a59584d to make the request