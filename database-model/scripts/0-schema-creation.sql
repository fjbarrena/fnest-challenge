-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema foundernest
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `foundernest` ;

-- -----------------------------------------------------
-- Schema foundernest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `foundernest` DEFAULT CHARACTER SET utf8 ;
USE `foundernest` ;

-- -----------------------------------------------------
-- Table `foundernest`.`criteria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`criteria` (
  `id` CHAR(36) NOT NULL DEFAULT (UUID()),
  `name` TINYTEXT NOT NULL,
  `createdDate` DATETIME NOT NULL DEFAULT NOW(),
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`company` (
  `id` CHAR(36) NOT NULL DEFAULT (UUID()),
  `name` VARCHAR(255) NOT NULL,
  `createdDate` DATETIME NOT NULL,
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`company_has_criteria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`company_has_criteria` (
  `company_id` CHAR(36) NOT NULL,
  `criteria_id` CHAR(36) NOT NULL,
  PRIMARY KEY (`company_id`, `criteria_id`),
  INDEX `fk_company_has_criteria_criteria1_idx` (`criteria_id` ASC) VISIBLE,
  INDEX `fk_company_has_criteria_company_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk_company_has_criteria_company`
    FOREIGN KEY (`company_id`)
    REFERENCES `foundernest`.`company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_has_criteria_criteria1`
    FOREIGN KEY (`criteria_id`)
    REFERENCES `foundernest`.`criteria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`user_company_matching`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`user_company_matching` (
  `id` CHAR(36) NOT NULL DEFAULT (UUID()),
  `company_id` CHAR(36) NOT NULL,
  `user_id` CHAR(36) NOT NULL,
  `matching` JSON NOT NULL,
  `createdDate` DATETIME NOT NULL DEFAULT NOW(),
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_company_matching_company1_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_company_matching_company1`
    FOREIGN KEY (`company_id`)
    REFERENCES `foundernest`.`company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`user_has_criteria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`user_has_criteria` (
  `criteria_id` CHAR(36) NOT NULL,
  `user_id` CHAR(36) NOT NULL,
  `type` ENUM('MUST', 'SUPER', 'NICE') NOT NULL,
  `createdDate` DATETIME NOT NULL DEFAULT NOW(),
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`criteria_id`, `user_id`),
  INDEX `fk_user_has_criteria_criteria1_idx` (`criteria_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_criteria_criteria1`
    FOREIGN KEY (`criteria_id`)
    REFERENCES `foundernest`.`criteria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`decision`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`decision` (
  `id` CHAR(36) NOT NULL DEFAULT (UUID()),
  `user_company_matching_id` CHAR(36) NOT NULL,
  `meet` TINYINT NOT NULL,
  `createdDate` DATETIME NOT NULL DEFAULT NOW(),
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_decision_user_company_matching1_idx` (`user_company_matching_id` ASC) VISIBLE,
  CONSTRAINT `fk_decision_user_company_matching1`
    FOREIGN KEY (`user_company_matching_id`)
    REFERENCES `foundernest`.`user_company_matching` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`reasons`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`reasons` (
  `id` CHAR(36) NOT NULL DEFAULT (UUID()),
  `reason` TINYTEXT NOT NULL,
  `type` ENUM('POSITIVE', 'NEGATIVE') NOT NULL,
  `createdDate` DATETIME NOT NULL DEFAULT NOW(),
  `lastModifiedDate` DATETIME NOT NULL,
  `lastModifiedUser` CHAR(36) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foundernest`.`decision_has_reasons`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foundernest`.`decision_has_reasons` (
  `decision_id` CHAR(36) NOT NULL,
  `reasons_id` CHAR(36) NOT NULL,
  PRIMARY KEY (`decision_id`, `reasons_id`),
  INDEX `fk_decision_has_reasons_reasons1_idx` (`reasons_id` ASC) VISIBLE,
  INDEX `fk_decision_has_reasons_decision1_idx` (`decision_id` ASC) VISIBLE,
  CONSTRAINT `fk_decision_has_reasons_decision1`
    FOREIGN KEY (`decision_id`)
    REFERENCES `foundernest`.`decision` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_decision_has_reasons_reasons1`
    FOREIGN KEY (`reasons_id`)
    REFERENCES `foundernest`.`reasons` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


/* DEFAULT DATA */
INSERT INTO `foundernest`.`criteria`
(`id`,
`name`,
`createdDate`,
`lastModifiedDate`,
`lastModifiedUser`)
VALUES
('a6e14029-b856-4645-9eb4-632a6a636386',
'CEO full-time',
'2021-08-03',
'2021-08-03',
UUID()),
('80bbc0c2-859a-4e33-9e35-1ccbaf300f8d',
'CTO full-time',
'2021-08-03',
'2021-08-03',
UUID()),
('52d29ce1-9a9b-4134-8dd5-0a7e067bd907',
'$1M <= Round < $10M',
'2021-08-03',
'2021-08-03',
UUID()),
('f6145971-ab03-49fb-aebe-72fae3ceded8',
'Source of leads is personal contact',
'2021-08-03',
'2021-08-03',
UUID()),
('7c3491cc-6508-4564-937c-b3a7e60decc6',
'CEO repeat entrepeneur',
'2021-08-03',
'2021-08-03',
UUID()),
('a587bf82-309f-4c70-a137-e9590a644a49',
'$50+K MRR',
'2021-08-03',
'2021-08-03',
UUID()),
('897759a3-4a46-4854-8936-862f56a6247f',
'CEO worked at high growth startup',
'2021-08-03',
'2021-08-03',
UUID()),
('3e2d3b6a-d0f5-46a5-98de-c462408f1db1',
'CTO with +2 years leading tech teams',
'2021-08-03',
'2021-08-03',
UUID()),
('1d4457fe-808b-483b-8c1f-a7bb30dad757',
'Previous investors from top founds',
'2021-08-03',
'2021-08-03',
UUID());

INSERT INTO `foundernest`.`user_has_criteria`
(`criteria_id`,
`user_id`,
`type`,
`createdDate`,
`lastModifiedDate`,
`lastModifiedUser`)
VALUES
('1d4457fe-808b-483b-8c1f-a7bb30dad757',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'NICE',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('897759a3-4a46-4854-8936-862f56a6247f',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'NICE',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('3e2d3b6a-d0f5-46a5-98de-c462408f1db1',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'NICE',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('a587bf82-309f-4c70-a137-e9590a644a49',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'SUPER',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('7c3491cc-6508-4564-937c-b3a7e60decc6',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'SUPER',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('f6145971-ab03-49fb-aebe-72fae3ceded8',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'SUPER',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('a6e14029-b856-4645-9eb4-632a6a636386',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'MUST',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('80bbc0c2-859a-4e33-9e35-1ccbaf300f8d',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'MUST',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d'),
('52d29ce1-9a9b-4134-8dd5-0a7e067bd907',
'05517a10-7e4d-4509-9ffb-57978a59584d',
'MUST',
'2021-08-03', '2021-08-03', '05517a10-7e4d-4509-9ffb-57978a59584d');
